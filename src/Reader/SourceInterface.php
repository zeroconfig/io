<?php
/**
 * Copyright MediaCT. All rights reserved.
 * https://www.mediact.nl
 */
namespace ZeroConfig\Io\Reader;

interface SourceInterface
{
    /**
     * Get an iterable source of data.
     *
     * @return iterable
     */
    public function __invoke(): iterable;
}
