<?php
/**
 * Copyright MediaCT. All rights reserved.
 * https://www.mediact.nl
 */
namespace ZeroConfig\Io\Reader;

use Iterator;

interface ReaderInterface extends Iterator
{
    /**
     * Get the current line.
     *
     * @return string
     */
    public function current(): string;
}
