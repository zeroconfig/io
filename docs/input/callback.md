# Callback resource

To process data from a custom resource, using a callable.

```php
<?php
use ZeroConfig\Io\Reader\CallbackResource;

/** @var callable $handle */
$reader = new CallbackResource($handle);

foreach ($reader as $line) {
    echo $line;
}
```

This can be useful to process data for which a generic resource is not available.
The following example shows how to use the callback reader to query a database
using [PDO](https://secure.php.net/manual/en/book.pdo.php).

```php
<?php
use ZeroConfig\Io\Reader\CallbackResource;
use ZeroConfig\Io\Transformer\String\LineEnding;
use ZeroConfig\Io\Writer\File;

/** @var PDO $connection */
$reader = new CallbackResource(
    function () use ($connection) : iterable {
        // Create a statement that fetches a list of users.
        $statement = $connection->query(
            'SELECT user FROM users',
            PDO::FETCH_ASSOC
        );
        
        // If the query had no result, return an empty list.
        if ($statement === false) {
            return [];
        }

        foreach ($statement as $result) {
            // Skip rows that lack the user column.
            if (!array_key_exists('user', $result)) {
                continue;
            }
            
            // Yield the user.
            yield $result['user'] . PHP_EOL;
        }
    }
);

// Store users in a text file.
$output = new File('users.txt');

// Store the users in the prepared output.
$output($filter($reader));
```

# Further reading

- [Input resources](../input.md)
