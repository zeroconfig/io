<?php
/**
 * Copyright MediaCT. All rights reserved.
 * https://www.mediact.nl
 */
namespace ZeroConfig\Io\Tests\Writer;

use PHPUnit\Framework\TestCase;
use ZeroConfig\Io\Writer\StandardOut;

/**
 * @coversDefaultClass \ZeroConfig\Io\Writer\StandardOut
 */
class StandardOutTest extends TestCase
{
    /**
     * @return void
     * @covers ::getHandle
     */
    public function testGetHandle(): void
    {
        $writer = new StandardOut();
        $this->assertInternalType('resource', $writer->getHandle());
    }
}
