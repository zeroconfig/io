<?php
/**
 * Copyright MediaCT. All rights reserved.
 * https://www.mediact.nl
 */
namespace ZeroConfig\Io\Tests\Writer;

use PHPUnit\Framework\TestCase;
use ZeroConfig\Io\Writer\StandardError;

/**
 * @coversDefaultClass \ZeroConfig\Io\Writer\StandardError
 */
class StandardErrorTest extends TestCase
{
    /**
     * @return void
     * @covers ::getHandle
     */
    public function testGetHandle(): void
    {
        $writer = new StandardError();
        $this->assertInternalType('resource', $writer->getHandle());
    }
}
