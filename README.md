# Introduction

ZeroConfig I/O is a set of input resources and output writers.

[![codecov](https://codecov.io/bb/zeroconfig/io/branch/master/graph/badge.svg)](https://codecov.io/bb/zeroconfig/io)
[![Packagist](https://img.shields.io/packagist/v/zero-config/io.svg)](https://packagist.org/packages/zero-config/io)
[![PHP from Packagist](https://img.shields.io/packagist/php-v/zero-config/io.svg)](https://secure.php.net/)
[![Packagist](https://img.shields.io/packagist/l/zero-config/io.svg)](https://github.com/ZeroConfig/io/blob/master/LICENSE)

By design, it solves how to deal with large data streams. It is based on data
going in, data being manipulated and then data going out. Whether the source is
piped data, a local file or HTTP resource, it will be streamed line by line.

# Installation

```
composer require zero-config/io
```

# I/O

The I/O is easily handled by the [input](docs/input.md) and [output](docs/output.md)
components of the package.

## Input

Input sources are implemented as generators and can thus be used to stream data
line by line.

```php
<?php
use ZeroConfig\Io\Reader\StandardIn;

$pipe = new StandardIn();

// Echo what is piped to the application.
foreach ($pipe as $line) {
    echo $line;
}
```

| Resource                      | Description                                            |
|:------------------------------|:-------------------------------------------------------|
| [File](docs/input/file.md)         | Read files from the local filesystem.                  |
| [Gzip](docs/input/gzip.md)         | Read Gzip archives, like backups of databases or logs. |
| [STDIN](docs/input/stdin.md)       | Read piped data streams.                               |
| [HTTP](docs/input/http.md)         | Stream web resources.                                  |
| [Callback](docs/input/callback.md) | Stream data using a callback.                          |

## Output

Output writers expect iterable data and are able to write data line by line;
ideal for handling streaming data.

```php
<?php
use ZeroConfig\Io\Writer\File;
use ZeroConfig\Io\Reader\ReaderInterface;

$writer = new File('The.Zookeeper\'s.Wife.mp4');

/** @var ReaderInterface $movie */
$writer($movie);
```

| Writer                                     | Description                              |
|:-------------------------------------------|:-----------------------------------------|
| [File](docs/output/file.md)                     | Write to a file on the local filesystem. |
| [STDOUT / STDERR](docs/output/stdout-stderr.md) | Write to the console.                    |
| [Callback](docs/output/callback.md)             | Write to a callable handle.              |
| [CSV](docs/output/csv.md)                       | Write to CSV files.                      |

# Guides

- [Downloading large files](docs/guides/downloading-large-files.md)
